import java.util.*;

public class AddMatDig{  
public static void main(String args[]){  
 
//int a[][]={{1,1,1},{2,2,2},{3,3,3}};

Scanner sc = new Scanner(System.in);
 
         int i,j,row,col,sum=0;
 System.out.println("Enter the number of rows:");
 row = sc.nextInt();
 System.out.println("Enter the number of columns:");
 col = sc.nextInt();
 

   int[][] a = new int[row][col];
 
     System.out.println("Enter the elements of the matrix") ;
     for(i=0;i<row;i++)
     { 
      for(j=0;j<col;j++)
      { 
          a[i][j] = sc.nextInt();
     }
 }


   for( i=0;i<row;i++)
     { 
      for( j=0;j<col;j++)
      { 
        System.out.print(a[i][j]+"\t");
     }
       System.out.println("");
 }


     int primary = 0, secondary = 0;
	 
        for ( i = 0; i < row ; i++) {
            primary += a[i][i];
            secondary += a[i][3 - i - 1];
        } 
        System.out.println("Primary Diagonal:" + primary);
                                    
        System.out.println("Secondary Diagonal:" + secondary);		
   
    
  } 
} 