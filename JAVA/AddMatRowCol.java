import java.util.*;

public class AddMatRowCol{  
public static void main(String args[]){  
 
Scanner sc = new Scanner(System.in);
 
         int i,j,row,col,SumRow = 0, SumCol = 0;
 System.out.println("Enter the number of rows:");
 row = sc.nextInt();
 System.out.println("Enter the number of columns:");
 col = sc.nextInt();
 

   int[][] a = new int[row][col];
 
     System.out.println("Enter the elements of the matrix") ;
     for(i=0;i<row;i++)
     { 
      for(j=0;j<col;j++)
      { 
          a[i][j] = sc.nextInt();
     }
 }


   for( i=0;i<row;i++)
     { 
      for( j=0;j<col;j++)
      { 
        System.out.print(a[i][j]+"\t");
     }
       System.out.println("");
 }

      // for Addition of rows

        for ( i = 0; i < row ; i++) {
			SumRow = 0;  
            for( j = 0; j < col; j++){    
              SumRow = SumRow + a[i][j];    
            }
        System.out.println("Sum of " + (i+1) +" row: " + SumRow);   			
        } 
		
		
		 // for Addition of columns

        for ( i = 0; i < col ; i++) {
			SumCol = 0;  
            for( j = 0; j < row; j++){    
              SumCol = SumCol + a[j][i];    
            }
        System.out.println("Sum of " + (i+1) +" col: " + SumCol);   			
        } 
		
       
    
  } 
} 