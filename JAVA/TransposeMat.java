import java.util.*;

public class TransposeMat{  
public static void main(String args[]){  
 
Scanner sc = new Scanner(System.in);
 
         int i,j,row,col,SumRow = 0, SumCol = 0;
 System.out.println("Enter the number of rows:");
 row = sc.nextInt();
 System.out.println("Enter the number of columns:");
 col = sc.nextInt();
 

   int[][] Org = new int[row][col];
 
     System.out.println("Enter the elements of the matrix") ;
     for(i=0;i<row;i++)
     { 
      for(j=0;j<col;j++)
      { 
          Org[i][j] = sc.nextInt();
     }
 }


   for( i=0;i<row;i++)
     { 
      for( j=0;j<col;j++)
      { 
        System.out.print(Org[i][j]+"\t");
     }
       System.out.println("");
 }



 

//creating another matrix to store transpose of a matrix  
	int trans[][]=new int[row][col];   
    

	// transpose from here 

		for( i=0;i<row;i++){    
			for( j=0;j<col;j++){    
				trans[i][j]=Org[j][i];  
			}    
		} 

		
      System.out.println(" Matrix After Transpose:");  
		for( i=0;i<row;i++){    
			for( j=0;j<col;j++){    
				System.out.print(trans[i][j]+" ");    
			}    
		  System.out.println();//new line    
		} 
    
  } 
} 