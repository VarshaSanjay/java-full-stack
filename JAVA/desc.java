/*Sort a  element array in descending order.*/

import java.util.*;
 class desc {
	 public static void main(String args[]){
		 
		 Scanner sc = new Scanner(System.in);
		 
		 
		 System.out.print("Enter Array length = ");
		 int len = sc.nextInt();
		 int arr[] = new int[len]; 
		
		 
		 
		 System.out.println("Enter Array elements = ");
		 for (int i = 0; i<len; i++)
		 {
		        arr[i] = sc.nextInt();
		 }
		 
		
		
		for(int i = 0;i<len; i++){
			  for(int j = i+1; j<len; j++){
				  
				  if(arr[j] > arr[i]){
					  int temp = arr[i];
					  arr[i]= arr[j];
					  arr[j] = temp;
				  }
			  }
		}
		System.out.println(" descending values are");
		for( int i : arr ) {
			
			
			
			System.out.println(i+" ");
			
		}
	 }
 
 }